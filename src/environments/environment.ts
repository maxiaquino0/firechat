// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC5MoJPRdt3MQ7jn5uzFwzxANGXkMR9YSc',
    authDomain: 'firechat-f76a0.firebaseapp.com',
    databaseURL: 'https://firechat-f76a0.firebaseio.com',
    projectId: 'firechat-f76a0',
    storageBucket: 'firechat-f76a0.appspot.com',
    messagingSenderId: '681251786113',
    appId: '1:681251786113:web:5ce152dfe3a79e5cddac80',
    measurementId: 'G-LY3VYSHV2Y'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
